SELECT internal_entry_id, external_entry_id, type, metadata
FROM entries
WHERE entries.internal_entry_id IN (:internal_entry_ids);