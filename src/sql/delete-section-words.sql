DELETE FROM section_words
WHERE section_id IN
(
    SELECT section_id
    FROM sections
    INNER JOIN entries ON sections.entry_id = entries.internal_entry_id
    WHERE entries.external_entry_id IN (:external_entry_ids)
);