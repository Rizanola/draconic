SELECT word_variants.word, word_variants.word_variant, word_variants.distance, section_words.word_index, section_words.character_index, section_words.section_id
FROM word_variants
INNER JOIN section_words on word_variants.word = section_words.word
INNER JOIN sections ON section_words.section_id = sections.section_id
INNER JOIN entries on sections.entry_id = entries.internal_entry_id
WHERE
(
    word_variants.word_variant IN (:word_variants)
    OR
    (
        word_variants.word_variant = word_variants.word
        AND word_variants.word IN (:exact_words)
    )
)
AND
(
	:type IS NULL
	OR entries.type = :type
)
ORDER BY section_words.section_id, section_words.word_index;