CREATE TABLE meta
(
    version INTEGER NOT NULL
);

CREATE TABLE entries
(
    internal_entry_id TEXT PRIMARY KEY NOT NULL,
    external_entry_id TEXT UNIQUE NOT NULL,
    type TEXT DEFAULT NULL,
    metadata TEXT NOT NULL DEFAULT '{}'
);

CREATE INDEX entries_external_entry_id ON entries(external_entry_id);
CREATE INDEX entries_type ON entries(type);

CREATE TABLE sections
(
    section_id TEXT PRIMARY KEY NOT NULL,
    priority INTEGER NOT NULL,
    label TEXT DEFAULT NULL,
    entry_id INTEGER NOT NULL
);

CREATE INDEX sections_entry_id ON sections(entry_id);

CREATE TABLE word_variants
(
    word TEXT NOT NULL,
    word_variant TEXT NOT NULL,
    distance INTEGER NOT NULL
);

CREATE INDEX word_variants_word ON word_variants(word);

CREATE TABLE section_words
(
    word TEXT NOT NULL,
    word_index INTEGER NOT NULL,
    character_index INTEGER NOT NULL,
    section_id INTEGER NOT NULL
);

CREATE INDEX section_words_section_id_word_index ON section_words(section_id, word_index);
CREATE INDEX section_words_word ON section_words(word);