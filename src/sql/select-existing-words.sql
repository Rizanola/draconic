SELECT DISTINCT word
FROM word_variants
WHERE word IN (:words);