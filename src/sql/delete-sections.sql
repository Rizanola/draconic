DELETE FROM sections
WHERE entry_id IN
(
    SELECT internal_entry_id
    FROM entries
    WHERE external_entry_id IN (:external_entry_ids)
);