DELETE FROM word_variants
WHERE word NOT IN
(
    SELECT word
    FROM section_words
);