<?php
	namespace Rizanola\Draconic;

	use Rizanola\Draconic\Database\DataModifier;
	use Rizanola\Draconic\Database\DataRetriever;
	use Rizanola\Draconic\Database\Exceptions\ConnectionException;
	use Rizanola\Draconic\Database\Exceptions\QueryException;
	use Rizanola\Draconic\Database\QueryRunning\QueryRunner;
	use Rizanola\Draconic\Database\QueryRunning\SqliteQueryRunner;
	use Rizanola\Draconic\Matching\Result;
	use Rizanola\Draconic\Words\Downcoding\Downcoder;
	use Rizanola\Draconic\Words\Downcoding\UrlifyDowncoder;
	use Rizanola\Draconic\Words\Stemming\NadarStemmer;
	use Rizanola\Draconic\Words\Stemming\Stemmer;
	use Rizanola\Draconic\Words\Word;
	use Rizanola\Draconic\Words\WordAlternator;
	use Rizanola\Draconic\Words\WordExtractor;
	use Rizanola\Draconic\Words\WordGroup;

	/**
	 * Base class for handling database modification and running queries
	 */
	class Draconic
	{
		private WordExtractor $wordExtractor;
		private DataModifier $dataModifier;
		private DataRetriever $dataRetriever;

		public int $typoDistance;
		public Stemmer $stemmer;
		public Downcoder $downcoder;

		/** @var string[]|null */
		private ?array $uniqueWords = null;

		/** @var string[]|null */
		private ?array $excludedWords = null;

		/** @var callable(array<Word|WordGroup|WordAlternator>, Result): bool */
		public $filterCallable;

		/** @var callable(array<Word|WordGroup|WordAlternator>, Result, Result): int */
		public $sortCallable;

		/**
		 * Creates a new draconic instance
		 * @param	string|QueryRunner		$database		The path to the database (or optionally a query runner). Takes anything that SQLite does, so it can be ":memory:" for an in-memory database, or an empty string for a temporary database stored on the disk.
		 * @param	int						$typoDistance	The number of typos that should be accounted for (will generate l^n variants, where l is the length of the word and n is the typo distance)
		 * @param	Stemmer					$stemmer		A stemmer to use for generating stem variants
		 * @param	Downcoder				$downcoder		A downcoder for downcoding the word
		 */
		public function __construct(string|QueryRunner $database, int $typoDistance = Word::DEFAULT_TYPO_DISTANCE, Stemmer $stemmer = new NadarStemmer(), Downcoder $downcoder = new UrlifyDowncoder())
		{
			if($database instanceof QueryRunner) $queryRunner = $database;
			else $queryRunner = new SqliteQueryRunner($database);

			$this->wordExtractor = new WordExtractor();
			$this->dataModifier = new DataModifier($queryRunner, $this->wordExtractor);
			$this->dataRetriever = new DataRetriever($queryRunner);

			$this->typoDistance = $typoDistance;
			$this->stemmer = $stemmer;
			$this->downcoder = $downcoder;

			$this->filterCallable = $this->filterResult(...);
			$this->sortCallable = $this->sortResults(...);
		}

		/**
		 * Adds or updates some entries into the database
		 * @param	iterable<Entry>		$entries	The entries to insert
		 * @throws	ConnectionException				If the database fails to connect
		 * @throws	QueryException					If something goes wrong with the query
		 */
		public function addOrUpdateEntries(iterable $entries): void
		{
			$this->dataModifier->addOrUpdateEntries($entries, $this->typoDistance, $this->stemmer, $this->downcoder);
		}

		/**
		 * Removes some entries from the database
		 * @param	iterable<Entry|int|float|string>	$entries	The entries to delete, or their IDs
		 * @throws	ConnectionException								If the database fails to connect
		 * @throws	QueryException									If something goes wrong with the query
		 */
		public function removeEntries(iterable $entries): void
		{
			$this->dataModifier->removeEntries($entries);
		}

		/**
		 * Runs a query against the database
		 * @param	string			$query	The query to run
		 * @param	string|null		$type	The category of entry to retrieve
		 * @return	Result[]				The results of the query
		 * @throws	ConnectionException		If the database fails to connect
		 * @throws	QueryException			If something goes wrong with the query
		 */
		public function search(string $query, ?string $type = null): array
		{
			$words = $this->wordExtractor->extract($query, true, true, true);
			$results = $this->dataRetriever->retrieveMatches($words, $type, $this->typoDistance, $this->stemmer, $this->downcoder);
			$results = array_filter($results, fn(Result $result) => ($this->filterCallable)($words, $result));
			usort($results, fn(Result $first, Result $second) => ($this->sortCallable)($words, $first, $second));
			$this->uniqueWords = null;
			$this->excludedWords = null;

			return $results;
		}

		/**
		 * Gets the unique words
		 * @param	array<Word|WordGroup|WordAlternator>	$words	The processed query
		 * @return	string[]										The unique words
		 */
		private function getUniqueWords(array $words): array
		{
			$normalWords = array_filter($words, fn(Word|WordGroup|WordAlternator $item) => $item instanceof Word && !$item->excluded);
			return array_unique(array_map(fn(Word $word) => $word->word, $normalWords));
		}

		/**
		 * Gets the excluded words
		 * @param	array<Word|WordGroup|WordAlternator>	$words	The processed query
		 * @return	string[]										The excluded words
		 */
		private function getExcludedWords(array $words): array
		{
			$excludedWords = array_filter($words, fn(Word|WordGroup|WordAlternator $item) => $item instanceof Word && $item->excluded);
			return array_unique(array_map(fn(Word $word) => $word->word, $excludedWords));
		}

		/**
		 * Filters a result
		 * @param	array<Word|WordGroup|WordAlternator>	$words		The processed query
		 * @param	Result									$result		The result to check
		 * @return	bool												Whether that result should be included
		 */
		public function filterResult(array $words, Result $result): bool
		{
			if($this->uniqueWords === null) $this->uniqueWords = $this->getUniqueWords($words);
			if(!$result->containsAll($this->uniqueWords)) return false;

			if($this->excludedWords === null) $this->excludedWords = $this->getExcludedWords($words);
			if($result->containsAny($this->excludedWords)) return false;

			foreach($words as $item)
			{
				if($item instanceof WordGroup)
				{
					if($item->excluded && $result->containsSequence($item)) return false;
					else if(!$item->excluded && !$result->containsSequence($item)) return false;
				}
			}

			return true;
		}

		/**
		 * Orders two results
		 * @param	array<Word|WordGroup|WordAlternator>	$words		The processed query
		 * @param	Result									$first		The first result to order
		 * @param	Result									$second		The second result to order
		 * @return	int													Negative for the first result first, 0 for equality, and positive for the second result first
		 */
		public function sortResults(array $words, Result $first, Result $second): int
		{
			if($this->uniqueWords === null) $this->uniqueWords = $this->getUniqueWords($words);

			$closenessComparison = $first->getExactnessScore() <=> $second->getExactnessScore(); // Lower score should come first
			if($closenessComparison !== 0) return $closenessComparison;

			$priorityComparison = $second->getPriorityScore() <=> $first->getPriorityScore(); // Higher score should come first
			if($priorityComparison !== 0) return $priorityComparison;

			return $second->getClosenessScore($this->uniqueWords) <=> $first->getClosenessScore($this->uniqueWords); // Higher score should come first
		}
	}