<?php
	namespace Rizanola\Draconic;

	use Rizanola\Draconic\Database\DatabaseItem;
	use Rizanola\Draconic\Words\Word;
	use Rizanola\Draconic\Words\WordExtractor;

	/**
	 * Holds details about a section to insert into the database
	 */
	class Section
	{
		use DatabaseItem;

		/**
		 * Creates a new section
		 * @param	string			$content	The section content
		 * @param	int				$priority	Results matching higher priority sections are placed higher in search results
		 * @param	string|null		$label		A label for this section. This gets returned alongside text snippets, but it's not required
		 */
		public function __construct(
			public string $content,
			public int $priority = 1,
			public ?string $label = null
		) {}

		/**
		 * Gets all the words in this section
		 * @param	WordExtractor	$wordExtractor	The word extractor to use
		 * @return	Word[]							The words
		 */
		public function getWords(WordExtractor $wordExtractor): array
		{
			/** @var Word[] $words */
			$words = $wordExtractor->extract($this->content, false, false, false);
			return $words;
		}
	}