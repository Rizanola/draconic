<?php
	namespace Rizanola\Draconic\Matching;

	use Rizanola\Draconic\Words\WordGroup;
	use stdClass;

	/**
	 * A single result that matches the query
	 */
	class Result
	{
		/**
		 * Creates a new result
		 * @param	string				$id					The ID of the entry
		 * @param	string|null			$type				The type of the entry
		 * @param	MatchedSection[]	$matchedSections	The sections that contain matched words
		 * @param	stdClass			$metadata			Metadata attached to the result
		 */
		public function __construct(
			public string $id,
			public ?string $type,
			public array $matchedSections,
			public stdClass $metadata = new stdClass()
		) {}

		/**
		 * Checks if this results contains any of the specified words
		 * @param	string[]	$words	The words to check
		 * @return	bool				Whether it contains any of them
		 */
		public function containsAny(array $words): bool
		{
			foreach($this->matchedSections as $matchedSection)
			{
				foreach($matchedSection->matchedWords as $matchedWord)
				{
					if(in_array($matchedWord->searchedWord, $words)) return true;
				}
			}

			return false;
		}

		/**
		 * Checks that this result contains all the specified words
		 * @param	string[]	$words	The words to check
		 * @return	bool				Whether it contains all of them
		 */
		public function containsAll(array $words): bool
		{
			$check = [];

			foreach($words as $word)
			{
				$check[$word] = false;
			}

			foreach($this->matchedSections as $matchedSection)
			{
				foreach($matchedSection->matchedWords as $matchedWord)
				{
					$check[$matchedWord->searchedWord] = true;
				}
			}

			foreach($check as $contains)
			{
				if(!$contains) return false;
			}

			return true;
		}

		/**
		 * Checks that this result contains a specific sequence of words
		 * @param	WordGroup	$group	The group of words
		 * @return	bool				Whether this group contains that sequence
		 */
		public function containsSequence(WordGroup $group): bool
		{
			foreach($this->matchedSections as $matchedSection)
			{
				if($matchedSection->containsSequence($group)) return true;
			}

			return false;
		}

		/**
		 * Gets the priority score for all the matches in this result
		 * @return	int		The priority score
		 */
		public function getPriorityScore(): int
		{
			$scores = array_map(fn(MatchedSection $section) => $section->getPriorityScore(), $this->matchedSections);
			return array_sum($scores);
		}

		/**
		 * Gets the exactness score for how close the words in this result match the ones originally searched for
		 * @return	float	The average exactness
		 */
		public function getExactnessScore(): float
		{
			$words = [];

			foreach($this->matchedSections as $section)
			{
				foreach($section->matchedWords as $matchedWord)
				{
					if(!isset($words[$matchedWord->searchedWord]) || $matchedWord->distance < $words[$matchedWord->searchedWord])
					{
						$words[$matchedWord->searchedWord] = $matchedWord->distance;
					}
				}
			}

			return array_sum($words) / count($words);
		}

		/**
		 * Gets the closeness score for how close the words in this result are to each other
		 * @param	string[]	$uniqueWords	Unique words that were originally searched for
		 * @return	float						The calculated closeness
		 */
		public function getClosenessScore(array $uniqueWords): float
		{
			$total = 0;

			for($i = 0; $i < count($uniqueWords); $i += 1)
			{
				for($j = $i + 1; $j < count($uniqueWords); $j += 1)
				{
					$closestDistance = null;

					foreach($this->matchedSections as $matchedSection)
					{
						$sectionDistance = $matchedSection->shortestDistanceBetween($uniqueWords[$i], $uniqueWords[$j]);
						if($sectionDistance === null) continue;
						if($closestDistance === null || $sectionDistance < $closestDistance) $closestDistance = $sectionDistance;
					}

					if($closestDistance === null || $closestDistance === 0) continue;
					$total += 1 / $closestDistance;
				}
			}

			return $total;
		}
	}