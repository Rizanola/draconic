<?php
	namespace Rizanola\Draconic\Matching;

	/**
	 * A single word in a result that matches a word in the query
	 */
	class MatchedWord
	{
		/**
		 * @param	string	$matchedWord		The word in the result that was matched
		 * @param	string	$searchedWord		The word that was actually searched for
		 * @param	int		$distance			The distance between the two words
		 * @param	int		$wordIndex			The index of the word in the section
		 * @param	int		$characterIndex		The index of the first character of the word in the section
		 */
		public function __construct
		(
			public string $matchedWord,
			public string $searchedWord,
			public int $distance,
			public int $wordIndex,
			public int $characterIndex
		) {}
	}