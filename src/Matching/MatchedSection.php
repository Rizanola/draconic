<?php
	namespace Rizanola\Draconic\Matching;

	use Rizanola\Draconic\Words\Word;
	use Rizanola\Draconic\Words\WordAlternator;
	use Rizanola\Draconic\Words\WordGroup;

	/**
	 * A single section of a matched result
	 */
	class MatchedSection
	{
		/**
		 * Creates a new matched section
		 * @param	int				$priority		The priority of the section
		 * @param	string|null		$label			A label for the section
		 * @param	MatchedWord[]	$matchedWords	The words that were matched
		 */
		public function __construct(
			public int $priority,
			public ?string $label,
			public array $matchedWords
		) {}

		/**
		 * Counts how many words this section matches
		 * @return	int		The number of matches
		 */
		public function countMatches(): int
		{
			$words = [];

			foreach($this->matchedWords as $matchedWord)
			{
				$words[] = $matchedWord->searchedWord;
			}

			return count(array_unique($words));
		}

		/**
		 * Multiplies match counts by priority
		 * @return	int		The score
		 */
		public function getPriorityScore(): int
		{
			return $this->priority * $this->countMatches();
		}

		/**
		 * Checks whether this section contains a specific sequence of words
		 * @param	WordGroup	$group	The sequence to check
		 * @return	bool				Whether that group is in this section
		 */
		public function containsSequence(WordGroup $group): bool
		{
			$matchedWords = array_filter($this->matchedWords, fn(MatchedWord $matchedWord) => $matchedWord->searchedWord === $matchedWord->matchedWord); // We only want to check for exact matches at this time
			usort($matchedWords, fn(MatchedWord $first, MatchedWord $second) => $first->wordIndex <=> $second->wordIndex);

			$doesMatch = function(Word|WordAlternator $search, MatchedWord $match)
			{
				foreach($search->getWords() as $word)
				{
					if($word->word === $match->searchedWord) return true;
				}

				return false;
			};

			$phraseWords = $group->words;

			// Iterate over all the words and check if it matches the first term
			foreach($matchedWords as $matchesIndex => $matchedWord)
			{
				if($doesMatch($phraseWords[0], $matchedWord))
				{
					// Iterate over the rest of the words. We've already checked the first word, so we start at index 1
					for($searchesIndex = 1; $searchesIndex < count($phraseWords); $searchesIndex += 1)
					{
						if(!isset($matchedWords[$matchesIndex + $searchesIndex])) continue(2);
						if($matchedWords[$matchesIndex + $searchesIndex]->wordIndex !== $matchedWord->wordIndex + $searchesIndex) continue(2);
						if(!$doesMatch($phraseWords[$searchesIndex], $matchedWords[$matchesIndex + $searchesIndex])) continue(2);
					}

					return true;
				}
			}

			return false;
		}

		/**
		 * Gets the shortest distance between two specific words
		 * @param	string		$first		The first word
		 * @param	string		$second		The second word
		 * @return	int|null				The shortest distance, or null if either of the words are missing
		 */
		public function shortestDistanceBetween(string $first, string $second): ?int
		{
			/** @var array<MatchedWord[]> $relevantMatches */
			$relevantMatches = [[], []];

			foreach($this->matchedWords as $matchedWord)
			{
				if($matchedWord->searchedWord === $first) $relevantMatches[0][] = $matchedWord;
				else if($matchedWord->searchedWord === $second) $relevantMatches[1][] = $matchedWord;
			}

			if(count($relevantMatches[0]) === 0 || count($relevantMatches[1]) === 0)
			{
				return null;
			}

			$lowestDistance = PHP_INT_MAX;

			foreach($relevantMatches[0] as $firstMatch)
			{
				foreach($relevantMatches[1] as $secondMatch)
				{
					$lowestDistance = min($lowestDistance, abs($firstMatch->wordIndex - $secondMatch->wordIndex));
				}
			}

			return $lowestDistance;
		}
	}