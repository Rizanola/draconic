<?php
	namespace Rizanola\Draconic\Words;

	use Rizanola\Draconic\Database\DatabaseItem;

	/**
	 * Contains a word and its distance from an origin word
	 */
	class Variant
	{
		use DatabaseItem;

		/**
		 * Creates a new variant
		 * @param	string	$variant	The word variant
		 * @param	int		$distance	The distance from the origin
		 */
		public function __construct(
			public string $variant,
			public int $distance
		) {}

		/**
		 * Outputs the variant as a string, mostly for use in array_unique()
		 * @return	string	The variant
		 */
		public function __toString(): string
		{
			return $this->variant;
		}
	}