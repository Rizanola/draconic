<?php
	namespace Rizanola\Draconic\Words;

	use Exception;
	use Rizanola\Draconic\Words\Downcoding\Downcoder;
	use Rizanola\Draconic\Words\Downcoding\UrlifyDowncoder;
	use Rizanola\Draconic\Words\Stemming\NadarStemmer;
	use Rizanola\Draconic\Words\Stemming\Stemmer;

	/**
	 * Represents a word in a string of text
	 */
	class Word implements Substring
	{
		public const DEFAULT_TYPO_DISTANCE = 1;
		private const MIN_VARIANT_LENGTH = 2;

		/**
		 * Creates a new word
		 * @param	string	$word				The word
		 * @param	int		$characterIndex		The index of the first character in the word in the original string
		 * @param	bool	$excluded			Whether this word should be excluded from search results
		 */
		public function __construct
		(
			public string $word,
			public int $characterIndex,
			public bool $excluded = false
		){}

		/**
		 * Normalises this word
		 * @return	Word	The normalised word
		 */
		public function normalise(): Word
		{
			$lowercase = strtolower($this->word);
			$periodless = str_replace(".", "", $lowercase);
			return new Word($periodless, $this->characterIndex);
		}

		/**
		 * Generates variants for this word
		 * @param	int				$typoDistance	The number of typos that should be accounted for (will generate l^n variants, where l is the length of the word and n is the typo distance)
		 * @param	Stemmer			$stemmer		A stemmer to use for generating stem variants
		 * @param	Downcoder		$downcoder		A downcoder for downcoding the word
		 * @return	Variant[]						The variants for this word
		 */
		public function generateVariants(int $typoDistance = Word::DEFAULT_TYPO_DISTANCE, Stemmer $stemmer = new NadarStemmer(), Downcoder $downcoder = new UrlifyDowncoder()): array
		{
			$variants = [new Variant($this->word, 0)];

			if(str_contains($this->word, "-"))
			{
				foreach(explode("-", $this->word) as $segment)
				{
					$variants[] = new Variant($segment, 0);
				}
			}

			if($stemmer !== null)
			{
				foreach($variants as $variant)
				{
					try
					{
						$stem = $stemmer->stem($variant->variant);
						if($stem === $variant->variant) continue;
						$variants[] = new Variant($stem, $variant->distance + 1);
					}
					catch(Exception)
					{
						// A lack of stem doesn't prevent this from working, so we'll just continue
					}
				}
			}

			foreach($variants as $variant)
			{
				$ascii = $downcoder->convert($variant->variant);
				if($ascii === $variant->variant) continue;
				$variants[] = new Variant($ascii, $variant->distance + 1);
			}

			$new = $variants;

			for($i = 0; $i < $typoDistance; $i += 1)
			{
				$previous = $new;
				$new = [];

				foreach($previous as $variant)
				{
					if(strlen($variant->variant) <= self::MIN_VARIANT_LENGTH) continue;

					for($j = 0; $j < strlen($variant->variant); $j += 1)
					{
						$variantString = substr($variant->variant, 0, $j) . substr($variant->variant, $j + 1);
						$new[] = new Variant($variantString, $variant->distance + 1);
					}
				}

				array_push($variants, ...$new);
			}

			return array_values(array_unique($variants));
		}

		//region Substring

		public function getCharacterIndex(): int
		{
			return $this->characterIndex;
		}

		public function getWords(): array
		{
			return [$this];
		}

		//endregion
	}