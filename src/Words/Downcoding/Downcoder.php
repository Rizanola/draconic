<?php
	namespace Rizanola\Draconic\Words\Downcoding;

	/**
	 * Interface for converting diacritical characters into ascii ones
	 */
	interface Downcoder
	{
		/**
		 * Converts diacritical characters
		 * @param	string	$input	The string to convert
		 * @return	string			The converted string
		 */
		public function convert(string $input): string;
	}