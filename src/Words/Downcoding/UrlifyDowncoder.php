<?php
	namespace Rizanola\Draconic\Words\Downcoding;

	use URLify;

	/**
	 * Downcodes using the URLify library
	 */
	class UrlifyDowncoder implements Downcoder
	{
		public function __construct(
			public string $languageCode = "en"
		) {}

		//region Downcoder

		public function convert(string $input): string
		{
			return URLify::downcode($input, $this->languageCode);
		}

		//endregion
	}