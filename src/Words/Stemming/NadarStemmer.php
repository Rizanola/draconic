<?php
	namespace Rizanola\Draconic\Words\Stemming;

	use Nadar\Stemming\Stemm;

	/**
	 * A stemmer using the Nadar stemming library
	 */
	class NadarStemmer implements Stemmer
	{
		public function __construct(
			private readonly string $language = "en"
		) {}

		//region Stemmer

		public function stem(string $word): string
		{
			return Stemm::stem($word, $this->language);
		}

		//endregion
	}