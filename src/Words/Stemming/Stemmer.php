<?php
	namespace Rizanola\Draconic\Words\Stemming;

	/**
	 * An object that can generate stems from a word
	 */
	interface Stemmer
	{
		/**
		 * Gets the stem of a word
		 * @param	string	$word	The word to get the stem from
		 * @return	string			The stem of that word
		 */
		public function stem(string $word): string;
	}