<?php
	namespace Rizanola\Draconic\Words\Stemming;

	use Exception;
	use Wamania\Snowball\NotFoundException;
	use Wamania\Snowball\Stemmer\Stemmer as WamaniaStemmerInterface;
	use Wamania\Snowball\StemmerFactory;

	/**
	 * A stemmer using the Wamania stemmer library
	 */
	class WamaniaStemmer implements Stemmer
	{
		private WamaniaStemmerInterface $stemmer;

		/**
		 * Creates a new stemmer
		 * @param	string	$language	The language to use
		 * @throws	NotFoundException	If that language doesn't exist
		 */
		public function __construct(string $language = "en")
		{
			$this->stemmer = StemmerFactory::create($language);
		}

		//region Stemmer

		/**
		 * @throws	Exception	If something goes wrong during stemming
		 */
		public function stem(string $word): string
		{
			return $this->stemmer->stem($word);
		}

		//endregion
	}