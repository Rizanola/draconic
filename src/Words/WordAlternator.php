<?php
	namespace Rizanola\Draconic\Words;

	/**
	 * Contains multiple words that can be substituted for one another
	 */
	class WordAlternator implements Substring
	{
		/**
		 * Creates a new word alternator
		 * @param	Word[]	$words	The alternate words
		 */
		public function __construct(
			public array $words
		) {}

		//region Substring

		public function getCharacterIndex(): int
		{
			return $this->words[0]->characterIndex;
		}

		public function getWords(): array
		{
			return $this->words;
		}

		//endregion
	}