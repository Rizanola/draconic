<?php
	namespace Rizanola\Draconic\Words;

	/**
	 * Handles extracting individual words from a string
	 */
	class WordExtractor
	{
		// Regex \w only supports ascii characters, while this includes all letters with diacritics
		private const WORD_CHARACTERS = "0-9A-zÀ-ÖØ-öø-įĴ-őŔ-žǍ-ǰǴ-ǵǸ-țȞ-ȟȤ-ȳɃɆ-ɏḀ-ẞƀ-ƓƗ-ƚƝ-ơƤ-ƥƫ-ưƲ-ƶẠ-ỿ";

		/**
		 * Given a string of words, returns an array containing those words. Most punctuation will be removed.
		 * @param	string									$string				The string to convert
		 * @param	bool									$groupExactWords	Whether to group words inside quotes into a WordGroup
		 * @param	bool									$excludeWords		Whether to exclude words that are preceded by a dash (-)
		 * @param	bool									$allowAlternates	Whether to allow alternate words
		 * @return	array<Word|WordAlternator|WordGroup>						The words and any subgroups to include
		 */
		public function extract(string $string, bool $groupExactWords, bool $excludeWords, bool $allowAlternates): array
		{
			// Convert all apostrophes (’) into single quotes, so that the system can handle them
			$string = str_replace("’", "'", $string);

			/** @var WordGroup[] $wordGroups */
			$wordGroups = [];

			// We need to extract exact substrings first
			if($groupExactWords && str_contains($string, '"'))
			{
				$pattern = $excludeWords ? '/-?".+?"/' : '/".+?"/';
				preg_match_all($pattern, $string, $matches, PREG_OFFSET_CAPTURE);

				/**
				 * @var string $substring
				 * @var int $characterIndex
				 */
				foreach($matches[0] as [$substring, $characterIndex])
				{
					$excluded = ($substring[0] === "-");
					if($excluded) $substring = substr($substring, 1);

					/** @var Word[] $words */
					$words = $this->extract($substring, false, false, true);
					$wordGroups[] = new WordGroup($words, $characterIndex, strlen($substring), $excluded);
				}
			}

			$wordCharacters = self::WORD_CHARACTERS;
			$punctuation = "'\\-";
			if($allowAlternates) $punctuation .= "|";

			$wordPattern = "[{$wordCharacters}]+([{$punctuation}][{$wordCharacters}]+)*";
			if($excludeWords) $wordPattern = "-?{$wordPattern}";

			$initialismPattern = "[{$wordCharacters}](\\.[$wordCharacters])+(?![$wordCharacters])";
			if($excludeWords) $initialismPattern = "-?{$initialismPattern}";

			$pattern = "/{$initialismPattern}|{$wordPattern}/";

			/** @var Word[] $words */
			$words = [];

			/** @var WordAlternator[] $wordAlternators */
			$wordAlternators = [];

			preg_match_all($pattern, $string, $matches, PREG_OFFSET_CAPTURE);

			foreach($matches[0] as [$word, $characterIndex])
			{
				$excluded = ($word[0] === "-");
				if($excluded) $word = substr($word, 1);

				// Words that are already part of a word group should be ignored
				foreach($wordGroups as $wordGroup)
				{
					if($characterIndex >= $wordGroup->characterIndex && $characterIndex < ($wordGroup->characterIndex + $wordGroup->length)) continue(2);
				}

				if(!str_contains($word, "|")) $words[] = new Word($word, $characterIndex, $excluded);
				else
				{
					$segments = explode("|", $word);
					$alternateWords = [];
					$length = 0;

					foreach($segments as $segment)
					{
						$alternateWords[] = new Word($segment, $characterIndex + $length, false);
						$length += strlen($segment) + 1; // +1 because we need to include the pipe (|)
					}

					$wordAlternators[] = new WordAlternator($alternateWords);
				}
			}

			$combined = array_merge($wordGroups, $words, $wordAlternators);
			usort($combined, fn(Substring $first, Substring $second) => $first->getCharacterIndex() <=> $second->getCharacterIndex());

			return $combined;
		}
	}