<?php
	namespace Rizanola\Draconic\Words;

	/**
	 * An interface to allow us to sort words, word groups and word alternators by character index
	 */
	interface Substring
	{
		/**
		 * Gets the character index of this substring
		 * @return	int		The character index
		 */
		public function getCharacterIndex(): int;

		/**
		 * Retrieves words from this substring
		 * @return	Word[]	The words
		 */
		public function getWords(): array;
	}