<?php
	namespace Rizanola\Draconic\Words;

	/**
	 * A group of words
	 */
	class WordGroup implements Substring
	{
		/**
		 * Creates a new array of words
		 * @param	array<Word|WordAlternator>	$words				The words in the group
		 * @param	int							$characterIndex		The index of the first character in the group
		 * @param	int							$length				The length of the characters in the group
		 * @param	bool						$excluded			Whether this word group should be excluded from search results
		 */
		public function __construct
		(
			public array $words,
			public int $characterIndex,
			public int $length,
			public bool $excluded = false
		){}

		//region Substring

		public function getCharacterIndex(): int
		{
			return $this->characterIndex;
		}

		public function getWords(): array
		{
			$words = [];

			foreach($this->words as $item)
			{
				array_push($words, ...$item->getWords());
			}

			return $words;
		}

		//endregion
	}