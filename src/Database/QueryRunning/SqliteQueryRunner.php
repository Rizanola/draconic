<?php
	namespace Rizanola\Draconic\Database\QueryRunning;

	use Exception;
	use Rizanola\Draconic\Database\Exceptions\ConnectionException;
	use Rizanola\Draconic\Database\Exceptions\QueryException;
	use Rizanola\Draconic\Database\QueryRetriever;
	use SQLite3;
	use Traversable;

	/**
	 * Runs queries on the database
	 */
	class SqliteQueryRunner implements QueryRunner
	{
		private const VERSION = 2;

		private string $databasePath;
		private QueryRetriever $queryRetriever;
		private ?SQLite3 $database = null;
		private bool $isReadOnly = true;

		/**
		 * Creates a new query runner
		 * @param	string			$databasePath		The path to the database
		 * @param	QueryRetriever	$queryRetriever		The thing that retrieves queries
		 */
		public function __construct(string $databasePath, QueryRetriever $queryRetriever = new QueryRetriever())
		{
			$this->databasePath = $databasePath;
			$this->queryRetriever = $queryRetriever;
		}

		/**
		 * Gets the database
		 * @param	bool		$needsWrite		Whether the query will need write access
		 * @return	SQLite3						The database
		 * @throws	ConnectionException			If the database fails to connect
		 * @throws	QueryException				If retrieving or updating the version fails
		 */
		public function getDatabase(bool $needsWrite): SQLite3
		{
			$connectTo = function(string $path, int $flags)
			{
				$database = new SQLite3($path, $flags);
				$database->enableExceptions(true);
				return $database;
			};
			
			$handleUpgrades = function(SQLite3 $database)
			{
				$version = $this->getVersion($database);
				if($version >= self::VERSION) return;
				if($this->isReadOnly) $database = $this->getDatabase(true); // This shouldn't cause unlimited recursion, but double check when doing refactoring
				$this->performUpgrade($database, $version);
			};

			try
			{
				if($this->database !== null)
				{
					if(!$needsWrite || !$this->isReadOnly) return $this->database;
					else
					{
						$this->database = $connectTo($this->databasePath, SQLITE3_OPEN_READWRITE);
						$this->isReadOnly = false;
						return $this->database;
					}
				}
				else if($this->databasePath === ":memory:" || $this->databasePath === "" || !file_exists($this->databasePath))
				{
					$this->database = $connectTo($this->databasePath, SQLITE3_OPEN_READWRITE | SQLITE3_OPEN_CREATE);
					$this->isReadOnly = false;

					$databaseVersion = self::VERSION;
					$initialisation = $this->queryRetriever->getQuery("setup-{$databaseVersion}");
					$this->database->query($initialisation);
					$this->runQueryOnDatabase($this->database, "insert-version", ["version" => self::VERSION]);

					return $this->database;
				}
				else if($needsWrite)
				{
					$this->database = $connectTo($this->databasePath, SQLITE3_OPEN_READWRITE);
					$this->isReadOnly = false;
					$handleUpgrades($this->database);
					assert($this->database !== null);
					return $this->database;
				}
				else
				{
					$this->database = $connectTo($this->databasePath, SQLITE3_OPEN_READONLY);
					$this->isReadOnly = true;
					$handleUpgrades($this->database);
					assert($this->database !== null);
					return $this->database;
				}
			}
			catch(QueryException $exception)
			{
				throw $exception;
			}
			catch(Exception $exception)
			{
				throw new ConnectionException($exception->getMessage(), $exception->getCode(), $exception);
			}
		}

		/**
		 * Gets the current version of the database
		 * @param	SQLite3		$database	The database to check
		 * @return	int						The database version
		 * @throws	QueryException			If something goes wrong while checking
		 */
		private function getVersion(SQLite3 $database): int
		{
			$rows = $this->runQueryOnDatabase($database, "get-version", []);
			if(count($rows) === 0) return 1;

			$version = $rows[0]["version"];
			assert(is_int($version));
			return $version;
		}

		/**
		 * Performs the required upgrades on the database
		 * @param	SQLite3		$database			The database to perform the upgrades on (must be writeable)
		 * @param	int			$currentVersion		The version the database is currently on
		 * @throws	QueryException					If something goes wrong while upgrading
		 */
		private function performUpgrade(SQLite3 $database, int $currentVersion): void
		{
			if($currentVersion < 2) $this->runQueryOnDatabase($database, "upgrade-to-2", []);

			if($currentVersion === 1) $this->runQueryOnDatabase($database, "insert-version", ["version" => self::VERSION]);
			else $this->runQueryOnDatabase($database, "set-version", ["version" => self::VERSION]);
		}

		//region QueryRunner

		public function queryMultiple(string $label, bool $requiresWrite, iterable $parameterGroups): void
		{
			$query = $this->queryRetriever->getQuery($label);
			$database = $this->getDatabase($requiresWrite);
			$transactionStarted = false;

			try
			{
				$statement = $database->prepare($query);
				assert($statement !== false); // SQLite should always throw exceptions on failure

				$database->exec("BEGIN TRANSACTION");
				$transactionStarted = true;

				foreach($parameterGroups as $parameters)
				{
					foreach($parameters as $placeholder => $parameter)
					{
						$statement->bindValue($placeholder, $parameter);
					}

					$statement->execute();
				}

				$database->exec("COMMIT TRANSACTION");
			}
			catch(Exception $exception)
			{
				if($transactionStarted) $database->exec("ROLLBACK TRANSACTION");
				throw new QueryException($exception->getMessage(), $exception->getCode(), $exception);
			}
		}

		/**
		 * Runs a query on a specific database
		 * @param	SQLite3						$database		The database to run the query on
		 * @param	string						$label			The label for the query
		 * @param	iterable<string, mixed>		$parameters		Map between placeholder names and parameters
		 * @return	array<string, mixed>[]						The result of the query
		 * @throws	QueryException								If the query fails to run
		 */
		private function runQueryOnDatabase(SQLite3 $database, string $label, iterable $parameters): array
		{
			$query = $this->queryRetriever->getQuery($label);

			if($parameters instanceof Traversable) $parameterArray = iterator_to_array($parameters);
			else $parameterArray = $parameters;

			try
			{
				if(count($parameterArray) === 0)
				{
					$result = $database->query($query);
				}
				else
				{
					// Some queries require a dynamic number of values in an IN() segment, so if a parameter consists of an array, we'll
					foreach($parameterArray as $placeholder => $parameter)
					{
						if(is_iterable($parameter))
						{
							if($placeholder[0] !== ":" && $placeholder[0] !== "@") $placeholder = ":{$placeholder}";

							$placeholders = [];
							$i = 0;

							foreach($parameter as $ignored)
							{
								$placeholders[] = "{$placeholder}_{$i}";
								$i += 1;
							}

							// We need at least one value in an IN() statement
							if(count($placeholders) === 0) $placeholders[] = "{$placeholder}_0";

							$placeholderString = implode(", ", $placeholders);
							$query = str_replace($placeholder, $placeholderString, $query);
						}
					}

					$statement = $database->prepare($query);
					assert($statement !== false); // SQLite should always throw exceptions on failure

					foreach($parameterArray as $placeholder => $parameter)
					{
						if($placeholder[0] !== ":" && $placeholder[0] !== "@") $placeholder = ":{$placeholder}";

						if(is_array($parameter))
						{
							// We need at least one value in an IN() statement, so we'll just use a unique ID. There's almost no way we could inadvertently wind up using it
							if(count($parameter) === 0) $parameter = [uniqid()];

							foreach($parameter as $index => $subParameter)
							{
								$statement->bindValue("{$placeholder}_{$index}", $subParameter);
							}
						}
						else
						{
							$statement->bindValue($placeholder, $parameter);
						}
					}

					$result = $statement->execute();
				}

				assert($result !== false); // SQLite should always throw exceptions on failure

				// This bit is important, since SQLite will casually _run the query twice_ if fetchArray() is called
				if($result->numColumns() === 0) return [];

				$rows = [];

				while(($row = $result->fetchArray(SQLITE3_ASSOC)) !== false)
				{
					$rows[] = $row;
				}

				return $rows;
			}
			catch(Exception $exception)
			{
				throw new QueryException($exception->getMessage(), $exception->getCode(), $exception);
			}
		}

		public function query(string $label, bool $requiresWrite, iterable $parameters = []): array
		{
			$database = $this->getDatabase($requiresWrite);
			return $this->runQueryOnDatabase($database, $label, $parameters);
		}

		//endregion
	}