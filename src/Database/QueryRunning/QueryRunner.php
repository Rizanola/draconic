<?php
	namespace Rizanola\Draconic\Database\QueryRunning;

	use Rizanola\Draconic\Database\Exceptions\ConnectionException;
	use Rizanola\Draconic\Database\Exceptions\QueryException;

	/**
	 * Handles running queries on the database
	 */
	interface QueryRunner
	{
		/**
		 * Runs a query multiple times
		 * @param	string								$label				The label for the query
		 * @param	bool								$requiresWrite		Whether the query requires write-access
		 * @param	iterable<iterable<string, mixed>>	$parameterGroups	The parameters for each iteration of the query
		 * @throws	ConnectionException										If the database fails to connect
		 * @throws	QueryException											If the query fails to run
		 */
		public function queryMultiple(string $label, bool $requiresWrite, iterable $parameterGroups): void;

		/**
		 * Runs a query
		 * @param	string						$label			The label for the query
		 * @param	bool						$requiresWrite	Whether the query requires write-access
		 * @param	iterable<string, mixed>		$parameters		Map between placeholder names and parameters
		 * @return	array<string, mixed>[]						The result of the query
		 * @throws	ConnectionException							If the database fails to connect
		 * @throws	QueryException								If the query fails to run
		 */
		public function query(string $label, bool $requiresWrite, iterable $parameters = []): array;
	}