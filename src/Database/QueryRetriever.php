<?php
	namespace Rizanola\Draconic\Database;

	use Error;

	/**
	 * Retrieves queries from storage
	 */
	class QueryRetriever
	{
		/** @var string[] */
		private array $queries;

		/**
		 * Gets a query from storage or the cache
		 * @param	string	$label	The query's label (the filename, without extension)
		 * @return	string			The requested query
		 */
		public function getQuery(string $label): string
		{
			$query = $this->queries[$label] ?? null;

			if($query === null)
			{
				$query = @file_get_contents(__DIR__ . "/../sql/{$label}.sql");
				if($query === false) throw new Error("Attempted to retrieve a non-existent SQL file: '{$label}'");
				$this->queries[$label] = $query;
			}

			return $query;
		}
	}