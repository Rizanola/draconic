<?php
	namespace Rizanola\Draconic\Database;

	use Ds\Map;
	use Rizanola\Draconic\Database\Exceptions\ConnectionException;
	use Rizanola\Draconic\Database\Exceptions\QueryException;
	use Rizanola\Draconic\Database\QueryRunning\QueryRunner;
	use Rizanola\Draconic\Matching\MatchedSection;
	use Rizanola\Draconic\Matching\MatchedWord;
	use Rizanola\Draconic\Matching\Result;
	use Rizanola\Draconic\Words\Downcoding\Downcoder;
	use Rizanola\Draconic\Words\Downcoding\UrlifyDowncoder;
	use Rizanola\Draconic\Words\Stemming\NadarStemmer;
	use Rizanola\Draconic\Words\Stemming\Stemmer;
	use Rizanola\Draconic\Words\Word;
	use Rizanola\Draconic\Words\WordAlternator;
	use Rizanola\Draconic\Words\WordGroup;

	/**
	 * Handles retrieving data from the database
	 */
	class DataRetriever
	{
		/**
		 * Creates a new data retriever
		 * @param	QueryRunner		$queryRunner	The thing that handles queries
		 */
		public function __construct(
			private readonly QueryRunner $queryRunner
		) {}

		/**
		 * Retrieves all related entities
		 * @param	iterable<Word|WordGroup|WordAlternator>		$words			The words to match
		 * @param	string|null									$type			The category of entry to retrieve
		 * @param	int											$typoDistance	The number of typos that should be accounted for (will generate l^n variants, where l is the length of the word and n is the typo distance)
		 * @param	Stemmer										$stemmer		A stemmer to use for generating stem variants
		 * @param	Downcoder									$downcoder		A downcoder for downcoding the word
		 * @return	Result[]													All word to word relationships
		 * @throws	ConnectionException											If the database fails to connect
		 * @throws	QueryException												If something goes wrong with the query
		 */
		public function retrieveMatches(iterable $words, ?string $type = null, int $typoDistance = Word::DEFAULT_TYPO_DISTANCE, Stemmer $stemmer = new NadarStemmer(), Downcoder $downcoder = new UrlifyDowncoder()): array
		{
			/** @var string[] $variantStrings */
			$variantStrings = [];

			/** @var string[] $exactStrings */
			$exactStrings = [];

			/** @var array<string, Word[]> $variantMap */
			$variantMap = [];

			/** @var Map<Word, array<string, int>> $wordMap */
			$wordMap = new Map();

			foreach($words as $item)
			{
				if($item instanceof WordGroup && !$item->excluded)
				{
					/** @var Word $word */
					foreach($item->getWords() as $word)
					{
						$wordMapping = $wordMap->get($word, []);

						$exactStrings[] = $word->word;

						$variantMap[$word->word][] = $word;
						$wordMapping[$word->word] = 0;

						$wordMap->put($word, $wordMapping);
					}
				}
				else
				{
					/** @var Word $word */
					foreach($item->getWords() as $word)
					{
						if($word->excluded) continue;
						$wordMapping = $wordMap->get($word, []);

						foreach($word->generateVariants($typoDistance, $stemmer, $downcoder) as $variant)
						{
							$variantStrings[] = $variant->variant;
							$variantMap[$variant->variant][] = $word;
							$wordMapping[$variant->variant] = $variant->distance;
						}

						$wordMap->put($word, $wordMapping);
					}
				}
			}

			$matchRows = $this->queryRunner->query("select-matching-words", false,
			[
				"type" => $type,
				"word_variants" => $variantStrings,
				"exact_words" => $exactStrings
			]);

			/** @var array<string, MatchedWord[]> $matchedWords */
			$matchedWords = [];

			/** @var array<string, MatchedSection[]> $matchedSections */
			$matchedSections = [];

			/** @var Result[] $results */
			$results = [];

			foreach($matchRows as $row)
			{
				$variantWords = $variantMap[$row["word_variant"]];

				foreach($variantWords as $word)
				{
					$distances = $wordMap->get($word);
					assert(is_string($row["word_variant"]) && $distances !== null);
					$distance = $distances[$row["word_variant"]];
					assert(is_string($row["word"]) && is_int($row["word_index"]) && is_int($row["character_index"]));
					$matchedWords[$row["section_id"]][] = new MatchedWord($row["word"], $word->word, $distance, $row["word_index"], $row["character_index"]);
				}
			}

			$sectionRows = $this->queryRunner->query("select-sections", false, ["section_ids" => array_keys($matchedWords)]);

			foreach($sectionRows as $row)
			{
				assert(is_int($row["priority"]) && (is_string($row["label"]) || $row["label"] === null));
				$matchedSections[$row["entry_id"]][] = new MatchedSection($row["priority"], $row["label"], $matchedWords[$row["section_id"]]);
			}

			$entryRows = $this->queryRunner->query("select-entries", false, ["internal_entry_ids" => array_keys($matchedSections)]);

			foreach($entryRows as $row)
			{
				assert(is_string($row["external_entry_id"]) && is_string($row["internal_entry_id"]) && (is_string($row["type"]) || $row["type"] === null) && is_string($row["metadata"]));
				$results[] = new Result($row["external_entry_id"], $row["type"], $matchedSections[$row["internal_entry_id"]], (object) json_decode($row["metadata"]));
			}

			return $results;
		}
	}