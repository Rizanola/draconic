<?php
	namespace Rizanola\Draconic\Database;

	use Ramsey\Uuid\Uuid;

	/**
	 * This trait is for items that need to be uniquely identified in the database
	 */
	trait DatabaseItem
	{
		private ?string $internalId = null;

		/**
		 * Gets this item's internal ID
		 */
		public function getInternalId(): string
		{
			$this->internalId ??= Uuid::uuid7();
			return $this->internalId;
		}
	}