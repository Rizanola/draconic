<?php
	namespace Rizanola\Draconic\Database;

	use Rizanola\Draconic\Database\Exceptions\ConnectionException;
	use Rizanola\Draconic\Database\Exceptions\QueryException;
	use Rizanola\Draconic\Database\QueryRunning\QueryRunner;
	use Rizanola\Draconic\Entry;
	use Rizanola\Draconic\Section;
	use Rizanola\Draconic\Words\Downcoding\Downcoder;
	use Rizanola\Draconic\Words\Downcoding\UrlifyDowncoder;
	use Rizanola\Draconic\Words\Stemming\NadarStemmer;
	use Rizanola\Draconic\Words\Stemming\Stemmer;
	use Rizanola\Draconic\Words\Word;
	use Rizanola\Draconic\Words\WordExtractor;

	/**
	 * Handles inserting and removing data from the database
	 */
	class DataModifier
	{
		/**
		 * Creates a new data modifier
		 * @param	QueryRunner		$queryRunner	The thing that handles queries
		 * @param	WordExtractor	$wordExtractor	A word extractor, for extracting words
		 */
		public function __construct(
			private readonly QueryRunner $queryRunner,
			private readonly WordExtractor $wordExtractor = new WordExtractor()
		) {}

		/**
		 * Inserts or replaces entries into the database
		 * @param	iterable<Entry>		$entries		The entries to insert
		 * @param	int					$typoDistance	The number of typos that should be accounted for (will generate l^n variants, where l is the length of the word and n is the typo distance)
		 * @param	Stemmer				$stemmer		A stemmer to use for generating stem variants
		 * @param	Downcoder			$downcoder		A downcoder for downcoding the word
		 * @throws	ConnectionException					If the database fails to connect
		 * @throws	QueryException						If something goes wrong with the query
		 */
		public function addOrUpdateEntries(iterable $entries, int $typoDistance = Word::DEFAULT_TYPO_DISTANCE, Stemmer $stemmer = new NadarStemmer(), Downcoder $downcoder = new UrlifyDowncoder()): void
		{
			$this->removeEntriesWithoutVariantRemoval($entries);

			$parameterGroups = [];

			foreach($entries as $entry)
			{
				$parameterGroups[] =
				[
					"internal_entry_id" => $entry->getInternalId(),
					"external_entry_id" => $entry->id,
					"type" => $entry->type,
					"metadata" => json_encode($entry->metadata)
				];
			}

			$this->queryRunner->queryMultiple("insert-entry", true, $parameterGroups);
			$this->addSections($entries, $typoDistance, $stemmer, $downcoder);
			$this->removeUnusedWordVariants();
		}

		/**
		 * Deletes entries from the database
		 * @param	iterable<Entry|int|float|string>	$entries	The entries to delete, or their IDs
		 * @throws	ConnectionException								If the database fails to connect
		 * @throws	QueryException									If something goes wrong with the query
		 */
		public function removeEntries(iterable $entries): void
		{
			$this->removeEntriesWithoutVariantRemoval($entries);
			$this->removeUnusedWordVariants();
		}

		/**
		 * Deletes entries from the database without removing word variants
		 * @param	iterable<Entry|int|float|string>	$entries	The entries to delete, or their IDs
		 * @throws	ConnectionException								If the database fails to connect
		 * @throws	QueryException									If something goes wrong with the query
		 */
		private function removeEntriesWithoutVariantRemoval(iterable $entries): void
		{
			$entryIds = [];

			foreach($entries as $entry)
			{
				$entryIds[] = ($entry instanceof Entry) ? $entry->id : $entry;
			}

			$this->removeSections($entryIds);
			$this->queryRunner->query("delete-entries", true, ["external_entry_ids" => $entryIds]);
		}

		/**
		 * Inserts a section into the database
		 * @param	iterable<Entry>		$entries		The entries containing the sections
		 * @param	int					$typoDistance	The number of typos that should be accounted for (will generate l^n variants, where l is the length of the word and n is the typo distance)
		 * @param	Stemmer				$stemmer		A stemmer to use for generating stem variants
		 * @param	Downcoder			$downcoder		A downcoder for downcoding the word
		 * @throws	ConnectionException					If the database fails to connect
		 * @throws	QueryException						If something goes wrong with the query
		 */
		private function addSections(iterable $entries, int $typoDistance, Stemmer $stemmer, Downcoder $downcoder): void
		{
			$parameterGroups = [];
			$sections = [];

			foreach($entries as $entry)
			{
				foreach($entry->sections as $section)
				{
					$sections[] = $section;

					$parameterGroups[] =
					[
						"section_id" => $section->getInternalId(),
						"priority" => $section->priority,
						"label" => $section->label,
						"entry_id" => $entry->getInternalId()
					];
				}
			}

			$this->queryRunner->queryMultiple("insert-sections", true, $parameterGroups);
			$this->addSectionWords($sections, $typoDistance, $stemmer, $downcoder);
		}

		/**
		 * Deletes all sections in the database that belong to some entries
		 * @param	iterable<int|float|string>	$entryIds	The IDs for the entries that the sections belong to
		 * @throws	ConnectionException						If the database fails to connect
		 * @throws	QueryException							If something goes wrong with the query
		 */
		private function removeSections(iterable $entryIds): void
		{
			$this->removeSectionWords($entryIds);
			$this->queryRunner->query("delete-sections", true, ["external_entry_ids" => $entryIds]);
		}

		/**
		 * Inserts the words from a group of sections into the database
		 * @param	iterable<Section>	$sections		The sections to insert the words for
		 * @param	int					$typoDistance	The number of typos that should be accounted for (will generate l^n variants, where l is the length of the word and n is the typo distance)
		 * @param	Stemmer				$stemmer		A stemmer to use for generating stem variants
		 * @param	Downcoder			$downcoder		A downcoder for downcoding the word
		 * @throws	ConnectionException					If the database fails to connect
		 * @throws	QueryException						If something goes wrong with the query
		 */
		private function addSectionWords(iterable $sections, int $typoDistance, Stemmer $stemmer, Downcoder $downcoder): void
		{
			$words = [];
			$parameterGroups = [];

			foreach($sections as $section)
			{
				$sectionWords = array_map(fn(Word $word) => $word->normalise(), $section->getWords($this->wordExtractor));
				array_push($words, ...$sectionWords);

				foreach($sectionWords as $index => $sectionWord)
				{
					$parameterGroups[] =
					[
						"word" => $sectionWord->word,
						"word_index" => $index,
						"character_index" => $sectionWord->characterIndex,
						"section_id" => $section->getInternalId()
					];
				}
			}

			$this->queryRunner->queryMultiple("insert-section-word", true, $parameterGroups);

			$wordStrings = array_values(array_unique(array_map(fn(Word $word) => $word->word, $words)));
			$existingWordRows = $this->queryRunner->query("select-existing-words", false, ["words" => $wordStrings]);
			$existingWords = array_map(fn(array $row) => $row["word"], $existingWordRows);

			$newWords = array_values(array_filter($words, fn(Word $word) => !in_array($word->word, $existingWords)));
			$this->addWordVariants($newWords, $typoDistance, $stemmer, $downcoder);
		}

		/**
		 * Removes the words from any section belonging to a group of entries from the database
		 * @param	iterable<int|float|string>	$entryIds	The IDs for the entries that the sections belong to
		 * @throws	ConnectionException						If the database fails to connect
		 * @throws	QueryException							If something goes wrong with the query
		 */
		private function removeSectionWords(iterable $entryIds): void
		{
			$this->queryRunner->query("delete-section-words", true, ["external_entry_ids" => $entryIds]);
		}

		/**
		 * Adds word variants into the database
		 * @param	iterable<Word>	$words			The words that the variants belong to
		 * @param	int				$typoDistance	The number of typos that should be accounted for (will generate l^n variants, where l is the length of the word and n is the typo distance)
		 * @param	Stemmer			$stemmer		A stemmer to use for generating stem variants
		 * @param	Downcoder		$downcoder		A downcoder for downcoding the word
		 * @throws	ConnectionException				If the database fails to connect
		 * @throws	QueryException					If something goes wrong with the query
		 */
		private function addWordVariants(iterable $words, int $typoDistance, Stemmer $stemmer, Downcoder $downcoder): void
		{
			$parameterGroups = [];

			foreach($words as $word)
			{
				foreach($word->generateVariants($typoDistance, $stemmer, $downcoder) as $variant)
				{
					$parameterGroups[] =
					[
						"word" => $word->word,
						"word_variant" => $variant->variant,
						"distance" => $variant->distance
					];
				}
			}

			$this->queryRunner->queryMultiple("insert-word-variant", true, $parameterGroups);
		}

		/**
		 * Removes unused word-variants from the database
		 * @throws	ConnectionException		If the database fails to connect
		 * @throws	QueryException			If something goes wrong with the query
		 */
		private function removeUnusedWordVariants(): void
		{
			$this->queryRunner->query("delete-word-variants", true);
		}
	}