<?php
	namespace Rizanola\Draconic\Database\Exceptions;

	use Exception;

	/**
	 * Thrown when something goes wrong during database actions
	 */
	abstract class DatabaseException extends Exception {}