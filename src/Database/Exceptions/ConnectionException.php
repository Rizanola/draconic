<?php
	namespace Rizanola\Draconic\Database\Exceptions;

	/**
	 * Thrown when something goes wrong while connecting to the database
	 */
	class ConnectionException extends DatabaseException {}