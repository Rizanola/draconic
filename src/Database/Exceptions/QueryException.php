<?php
	namespace Rizanola\Draconic\Database\Exceptions;

	/**
	 * Thrown when something goes wrong while running a query
	 */
	class QueryException extends DatabaseException {}