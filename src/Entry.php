<?php
	namespace Rizanola\Draconic;

	use Rizanola\Draconic\Database\DatabaseItem;
	use stdClass;

	/**
	 * Holds details about an entry to insert into or update in the database
	 */
	class Entry
	{
		use DatabaseItem;

		public int|float|string $id;
		public ?string $type;
		public readonly stdClass $metadata;

		/** @var Section[] */
		public array $sections;

		/**
		 * Creates a new entry
		 * @param	int|float|string	$id				The unique ID for this entry. This is what gets returned when doing a search
		 * @param	string|null			$type			A category for this entry, which can be used to filter results. e.g. "product" or "article".
		 * @param	Section				...$sections	The individual sections belonging to this entry
		 */
		public function __construct(int|float|string $id, ?string $type, Section ...$sections)
		{
			$this->id = $id;
			$this->type = $type;
			$this->sections = $sections;
			$this->metadata = new stdClass();
		}

		/**
		 * Sets a metadata value
		 * @param	string	$key	The metadata key
		 * @param	mixed	$value	The metadata value. This will be JSON encoded, so it can be anything except a resource
		 */
		public function setMetadata(string $key, mixed $value): void
		{
			$this->metadata->$key = $value;
		}

		/**
		 * Unsets a metadata value
		 * @param	string	$key	The metadata key
		 */
		public function unsetMetadata(string $key): void
		{
			unset($this->metadata->$key);
		}
	}