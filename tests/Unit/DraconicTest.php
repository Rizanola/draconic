<?php
	namespace Unit;

	use PHPUnit\Framework\TestCase;
	use Rizanola;
	use Rizanola\Draconic\Draconic;
	use Rizanola\Draconic\Matching\MatchedSection;
	use Rizanola\Draconic\Matching\MatchedWord;
	use Rizanola\Draconic\Matching\Result;
	use Rizanola\Draconic\Words\WordExtractor;

	class DraconicTest extends TestCase
	{
		/**
		 * Tests that results without all matching words get filtered out
		 */
		public function testFilterResultSomeMatchingWords(): void
		{
			$wordExtractor = new WordExtractor();
			$draconic = new Draconic("");

			$words = $wordExtractor->extract("red wolf", true, true, true);

			$result = new Result("red-fox", null, [new MatchedSection(1, null, [new MatchedWord("red", "red", 0, 0, 0)])]);

			$this->assertFalse($draconic->filterResult($words, $result));
		}

		/**
		 * Tests that results with all matching words are included
		 */
		public function testFilterResultAllMatchingWords(): void
		{
			$wordExtractor = new WordExtractor();
			$draconic = new Draconic("");

			$words = $wordExtractor->extract("red fox", true, true, true);

			$result = new Result("red-fox", null, [new MatchedSection(1, null, [new MatchedWord("red", "red", 0, 0, 0), new MatchedWord("fox", "fox", 0, 1, 4)])]);

			$this->assertTrue($draconic->filterResult($words, $result));
		}

		/**
		 * Tests that results with excluded words aren't included
		 */
		public function testFilterResultNoExcludedWords(): void
		{
			$wordExtractor = new WordExtractor();
			$draconic = new Draconic("");

			$words = $wordExtractor->extract("red -fox", true, true, true);

			$result = new Result("red-fox", null, [new MatchedSection(1, null, [new MatchedWord("red", "red", 0, 0, 0), new MatchedWord("fox", "fox", 0, 1, 4)])]);

			$this->assertFalse($draconic->filterResult($words, $result));
		}

		/**
		 * Tests that results with one alternate are included
		 */
		public function testFilterResultOneAlternate(): void
		{
			$wordExtractor = new WordExtractor();
			$draconic = new Draconic("");

			$words = $wordExtractor->extract("red|blue", true, true, true);

			$result = new Result("red-fox", null, [new MatchedSection(1, null, [new MatchedWord("red", "red", 0, 0, 0), new MatchedWord("fox", "fox", 0, 1, 4)])]);

			$this->assertTrue($draconic->filterResult($words, $result));
		}

		/**
		 * Tests that results with no alternatives are included
		 */
		public function testFilterResultNoAlternates(): void
		{
			$wordExtractor = new WordExtractor();
			$draconic = new Draconic("");

			$words = $wordExtractor->extract("blue|green", true, true, true);

			$result = new Result("red-fox", null, [new MatchedSection(1, null, [new MatchedWord("red", "red", 0, 0, 0), new MatchedWord("fox", "fox", 0, 1, 4)])]);

			$this->assertTrue($draconic->filterResult($words, $result));
		}

		/**
		 * Tests that results without an exactly matching sequence of words are filtered out
		 */
		public function testFilterResultIncorrectSequence(): void
		{
			$wordExtractor = new WordExtractor();
			$draconic = new Draconic("");

			$words = $wordExtractor->extract('"fox red"', true, true, true);

			$result = new Result("red-fox", null, [new MatchedSection(1, null, [new MatchedWord("red", "red", 0, 0, 0), new MatchedWord("fox", "fox", 0, 1, 4)])]);

			$this->assertFalse($draconic->filterResult($words, $result));
		}

		/**
		 * Tests that results with an exactly matching sequence of words are included
		 */
		public function testFilterResultCorrectSequence(): void
		{
			$wordExtractor = new WordExtractor();
			$draconic = new Draconic("");

			$words = $wordExtractor->extract('"red fox"', true, true, true);

			$result = new Result("red-fox", null, [new MatchedSection(1, null, [new MatchedWord("red", "red", 0, 0, 0), new MatchedWord("fox", "fox", 0, 1, 4)])]);

			$this->assertTrue($draconic->filterResult($words, $result));
		}

		/**
		 * Tests that results with an exact sequence that contains an alternate is included
		 */
		public function testFilterResultCorrectSequenceWithAlternate(): void
		{
			$wordExtractor = new WordExtractor();
			$draconic = new Draconic("");

			$words = $wordExtractor->extract('"red|blue fox"', true, true, true);

			$result = new Result("red-fox", null, [new MatchedSection(1, null, [new MatchedWord("red", "red", 0, 0, 0), new MatchedWord("fox", "fox", 0, 1, 4)])]);

			$this->assertTrue($draconic->filterResult($words, $result));
		}

		/**
		 * Tests that results with more matches in a higher priority section are ordered first
		 */
		public function testSortResultsHigherPriorityFirst(): void
		{
			$wordExtractor = new WordExtractor();
			$draconic = new Draconic("");

			$words = $wordExtractor->extract('red fox', true, true, true);

			$result1 = new Result("red-fox", null, [new MatchedSection(1, null, [new MatchedWord("red", "red", 0, 0, 0), new MatchedWord("fox", "fox", 0, 1, 4)])]);

			$result2 = new Result("red-fox", null, [new MatchedSection(2, null, [new MatchedWord("red", "red", 0, 0, 0), new MatchedWord("fox", "fox", 0, 1, 4)])]);

			$this->assertGreaterThan(0, $draconic->sortResults($words, $result1, $result2));
		}

		/**
		 * Tests that results with words that are closer together are ordered first
		 */
		public function testSortResultsCloserWordsFirst(): void
		{
			$wordExtractor = new WordExtractor();
			$draconic = new Draconic("");

			$words = $wordExtractor->extract('little fox', true, true, true);

			$result1 = new Result("little-red-fox", null, [new MatchedSection(1, null, [new MatchedWord("little", "little", 0, 0, 0), new MatchedWord("fox", "fox", 0, 2, 11)])]);

			$result2 = new Result("little-fox", null, [new MatchedSection(1, null, [new MatchedWord("little", "little", 0, 0, 0), new MatchedWord("fox", "fox", 0, 1, 7)])]);

			$this->assertGreaterThan(0, $draconic->sortResults($words, $result1, $result2));
		}

		/**
		 * Tests that results with more exact wording is ordered first
		 */
		public function testSortResultsMoreExactFirst(): void
		{
			$wordExtractor = new WordExtractor();
			$draconic = new Draconic("");

			$words = $wordExtractor->extract('red fox', true, true, true);

			$result1 = new Result("rad-fox", null, [new MatchedSection(1, null, [new MatchedWord("rad", "red", 1, 0, 0), new MatchedWord("fox", "fox", 0, 1, 4)])]);

			$result2 = new Result("red-fox", null, [new MatchedSection(1, null, [new MatchedWord("red", "red", 0, 0, 0), new MatchedWord("fox", "fox", 0, 1, 4)])]);

			$this->assertGreaterThan(0, $draconic->sortResults($words, $result1, $result2));
		}
	}