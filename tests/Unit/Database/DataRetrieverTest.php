<?php
	namespace Unit\Database;

	use PHPUnit\Framework\TestCase;
	use Rizanola\Draconic\Database\DataModifier;
	use Rizanola\Draconic\Database\DataRetriever;
	use Rizanola\Draconic\Database\Exceptions\ConnectionException;
	use Rizanola\Draconic\Database\Exceptions\QueryException;
	use Rizanola\Draconic\Database\QueryRunning\SqliteQueryRunner;
	use Rizanola\Draconic\Entry;
	use Rizanola\Draconic\Section;
	use Rizanola\Draconic\Words\WordExtractor;

	class DataRetrieverTest extends TestCase
	{
		private SqliteQueryRunner $queryRunner;

		/**
		 * @throws    ConnectionException        If the database fails to connect
		 * @throws    QueryException            If something goes wrong with the query
		 */
		protected function setUp(): void
		{
			parent::setUp();

			$this->queryRunner = new SqliteQueryRunner(":memory:");
			$dataModifier = new DataModifier($this->queryRunner);

			$entry1 = new Entry("test-1", "type-1", new Section("Canid - White Fox"), new Section("A fox with white fur"));
			$entry2 = new Entry("test-2", "type-2", new Section("Canid - Red Fox"), new Section("A fox with red fur"));

			$dataModifier->addOrUpdateEntries([$entry1, $entry2]);
		}

		/**
		 * Tests that matching no words returns no entries
		 * @throws    ConnectionException        If the database fails to connect
		 * @throws    QueryException            If something goes wrong with the query
		 */
		public function testRetrieveMatchesNoWords(): void
		{
			$wordExtractor = new WordExtractor();
			$query = "blue wolf";

			$dataRetriever = new DataRetriever($this->queryRunner);
			$results = $dataRetriever->retrieveMatches($wordExtractor->extract($query, true, true, true));

			$this->assertCount(0, $results);
		}

		/**
		 * Tests that a matching word returns both entries
		 * @throws    ConnectionException        If the database fails to connect
		 * @throws    QueryException            If something goes wrong with the query
		 */
		public function testRetrieveMatchesSomeWords(): void
		{
			$wordExtractor = new WordExtractor();
			$query = "fox";

			$dataRetriever = new DataRetriever($this->queryRunner);
			$results = $dataRetriever->retrieveMatches($wordExtractor->extract($query, true, true, true));

			$this->assertCount(2, $results);
		}

		/**
		 * Tests that variant matching will work
		 * @throws    ConnectionException        If the database fails to connect
		 * @throws    QueryException            If something goes wrong with the query
		 */
		public function testRetrieveMatchesWithVariants(): void
		{
			$wordExtractor = new WordExtractor();
			$query = "rod";

			$dataRetriever = new DataRetriever($this->queryRunner);
			$results = $dataRetriever->retrieveMatches($wordExtractor->extract($query, true, true, true));

			$this->assertCount(1, $results);
		}

		/**
		 * Tests that exact matches won't use variants
		 * @throws    ConnectionException        If the database fails to connect
		 * @throws    QueryException            If something goes wrong with the query
		 */
		public function testRetrieveMatchesWithoutVariants(): void
		{
			$wordExtractor = new WordExtractor();
			$query = '"rod"';

			$dataRetriever = new DataRetriever($this->queryRunner);
			$results = $dataRetriever->retrieveMatches($wordExtractor->extract($query, true, true, true));

			$this->assertCount(0, $results);
		}

		/**
		 * Tests that matches will be filtered by type
		 * @throws    ConnectionException        If the database fails to connect
		 * @throws    QueryException            If something goes wrong with the query
		 */
		public function testRetrieveMatchesByType(): void
		{
			$wordExtractor = new WordExtractor();
			$query = "fox";

			$dataRetriever = new DataRetriever($this->queryRunner);
			$results = $dataRetriever->retrieveMatches($wordExtractor->extract($query, true, true, true), "type-1");

			$this->assertCount(1, $results);
		}
	}