<?php
	namespace Unit\Database;

	use PHPUnit\Framework\Attributes\Depends;
	use PHPUnit\Framework\TestCase;
	use Rizanola\Draconic\Database\DataModifier;
	use Rizanola\Draconic\Database\Exceptions\ConnectionException;
	use Rizanola\Draconic\Database\Exceptions\QueryException;
	use Rizanola\Draconic\Database\QueryRunning\SqliteQueryRunner;
	use Rizanola\Draconic\Entry;
	use Rizanola\Draconic\Section;
	use Rizanola\Draconic\Words\WordExtractor;
	use SQLite3;

	class DataModifierTest extends TestCase
	{
		/**
		 * Runs a query on a database and returns an array of results
		 * @param	SQLite3					$database	The database to run the query on
		 * @param	string					$query		The query to run
		 * @return	array<string, mixed>[]				The results
		 */
		private function runQuery(SQLite3 $database, string $query): array
		{
			$result = $database->query($query);
			assert($result !== false);
			$rows = [];

			while(($row = $result->fetchArray(SQLITE3_ASSOC)) !== false)
			{
				$rows[] = $row;
			}

			return $rows;
		}

		/**
		 * Tests that inserting an entry works correctly
		 * @return    SqliteQueryRunner                The query runner that was used to insert the entry
		 * @throws    ConnectionException        If something goes wrong while connecting to the in-memory database
		 * @throws    QueryException            If something goes wrong with a query
		 */
		public function testAddEntry(): SqliteQueryRunner
		{
			$entry = new Entry("test", null, new Section("This is a heading", 2, "heading"), new Section("This is some content", 1, "content"));

			$queryRunner = new SqliteQueryRunner(":memory:");
			$dataModifier = new DataModifier($queryRunner, new WordExtractor());
			$dataModifier->addOrUpdateEntries([$entry]);
			$database = $queryRunner->getDatabase(false);

			$countEntries = $this->runQuery($database, "SELECT COUNT(internal_entry_id) AS entry_count FROM entries");
			$this->assertEquals(1, $countEntries[0]["entry_count"]);

			$countSections = $this->runQuery($database, "SELECT COUNT(section_id) AS section_count FROM sections");
			$this->assertEquals(2, $countSections[0]["section_count"]);

			$countSectionWords = $this->runQuery($database, "SELECT COUNT(word) AS section_word_count FROM section_words");
			$this->assertEquals(8, $countSectionWords[0]["section_word_count"]);

			return $queryRunner;
		}

		/**
		 * Tests that replacing an entry works correctly
		 * @param SqliteQueryRunner $queryRunner The query runner that was used to insert the previous entry
		 * @return    SqliteQueryRunner                        The query runner that was used to replace the entry
		 * @throws    ConnectionException                If something goes wrong while connecting to the in-memory database
		 * @throws    QueryException                    If something goes wrong with a query
		 */
		#[Depends("testAddEntry")]
		public function testReplaceEntry(SqliteQueryRunner $queryRunner): SqliteQueryRunner
		{
			$entry = new Entry("test", null, new Section("This is content", 1, "content"));

			$dataModifier = new DataModifier($queryRunner, new WordExtractor());
			$dataModifier->addOrUpdateEntries([$entry]);
			$database = $queryRunner->getDatabase(false);

			$countEntries = $this->runQuery($database, "SELECT COUNT(internal_entry_id) AS entry_count FROM entries");
			$this->assertEquals(1, $countEntries[0]["entry_count"]);

			$countSections = $this->runQuery($database, "SELECT COUNT(section_id) AS section_count FROM sections");
			$this->assertEquals(1, $countSections[0]["section_count"]);

			$countSectionWords = $this->runQuery($database, "SELECT COUNT(word) AS section_word_count FROM section_words");
			$this->assertEquals(3, $countSectionWords[0]["section_word_count"]);

			return $queryRunner;
		}

		/**
		 * Tests that deleting an entry works correctly
		 * @param SqliteQueryRunner $queryRunner The query runner that was used to replace the previous entry
		 * @throws    ConnectionException                If something goes wrong while connecting to the in-memory database
		 * @throws    QueryException                    If something goes wrong with a query
		 */
		#[Depends("testReplaceEntry")]
		public function testRemoveEntry(SqliteQueryRunner $queryRunner): void
		{
			$dataModifier = new DataModifier($queryRunner, new WordExtractor());
			$dataModifier->removeEntries(["test"]);
			$database = $queryRunner->getDatabase(false);

			$countEntries = $this->runQuery($database, "SELECT COUNT(internal_entry_id) AS entry_count FROM entries");
			$this->assertEquals(0, $countEntries[0]["entry_count"]);

			$countSections = $this->runQuery($database, "SELECT COUNT(section_id) AS section_count FROM sections");
			$this->assertEquals(0, $countSections[0]["section_count"]);

			$countSectionWords = $this->runQuery($database, "SELECT COUNT(word) AS section_word_count FROM section_words");
			$this->assertEquals(0, $countSectionWords[0]["section_word_count"]);

			$countSectionWords = $this->runQuery($database, "SELECT COUNT(word_variant) AS word_variant_count FROM word_variants");
			$this->assertEquals(0, $countSectionWords[0]["word_variant_count"]);
		}
	}