<?php
	namespace Unit\Words;

	use PHPUnit\Framework\TestCase;
	use Rizanola\Draconic\Words\Word;
	use Rizanola\Draconic\Words\WordAlternator;
	use Rizanola\Draconic\Words\WordExtractor;
	use Rizanola\Draconic\Words\WordGroup;

	class WordExtractorTest extends TestCase
	{
		/**
		 * Converts words and word groups into an array, for easier comparison
		 * @param	array<Word|WordGroup|WordAlternator>	$words	The words to convert
		 * @return	array<string|string[]>            				The converted words
		 */
		private function convertWords(array $words): array
		{
			return array_map(function(Word|WordGroup|WordAlternator $item)
			{
				if($item instanceof Word) return $item->word;
				else return array_map(fn(Word $word) => $word->word, $item->getWords());
			}, $words);
		}

		/**
		 * Tests that an empty string returns an empty array
		 */
		public function testExtractEmpty(): void
		{
			$wordExtractor = new WordExtractor();
			$this->assertCount(0, $wordExtractor->extract("", true, true, true));
		}

		/**
		 * Tests that a single word string returns an array containing one word
		 */
		public function testExtractOneWord(): void
		{
			$wordExtractor = new WordExtractor();
			$result = $this->convertWords($wordExtractor->extract("Extraction", true, true, true));
			$this->assertEquals(["Extraction"], $result);
		}

		/**
		 * Test that a multi-word string returns an array containing those words
		 */
		public function testExtractMultipleWords(): void
		{
			$wordExtractor = new WordExtractor();
			$result = $this->convertWords($wordExtractor->extract("Extraction Successful", true, true, true));
			$this->assertEquals(["Extraction", "Successful"], $result);
		}

		/**
		 * Tests that a word containing an apostrophe is correctly extracted
		 */
		public function testExtractWithApostrophe(): void
		{
			$wordExtractor = new WordExtractor();
			$result = $this->convertWords($wordExtractor->extract("Can't", true, true, true));
			$this->assertEquals(["Can't"], $result);
		}

		/**
		 * Tests that an initialism is correctly extracted
		 */
		public function testExtractInitialism(): void
		{
			$wordExtractor = new WordExtractor();
			$result = $this->convertWords($wordExtractor->extract("E.I.D", true, true, true));
			$this->assertEquals(["E.I.D"], $result);
		}

		/**
		 * Tests that a string missing a space after the full stop is not treated as an initialism
		 */
		public function testExtractFullStopWithMissingSpace(): void
		{
			$wordExtractor = new WordExtractor();
			$result = $this->convertWords($wordExtractor->extract("Hello.How are you?", true, true, true));
			$this->assertEquals(["Hello", "How", "are", "you"], $result);
		}

		/**
		 * Tests that non-word punctuation is stripped from the returned words
		 */
		public function testExtractWithPunctuation(): void
		{
			$wordExtractor = new WordExtractor();
			$result = $this->convertWords($wordExtractor->extract("'This, is working!'", true, true, true));
			$this->assertEquals(["This", "is", "working"], $result);
		}

		/**
		 * Tests that single quotes are stripped from outside words, while apostrophes remain
		 */
		public function testExtractWithApostropheInsideSingleQuotes(): void
		{
			$wordExtractor = new WordExtractor();
			$result = $this->convertWords($wordExtractor->extract("'Don't'", true, true, true));
			$this->assertEquals(["Don't"], $result);
		}

		/**
		 * Tests that words with diacritics are extracted, and retain their diacritics after extraction
		 */
		public function testExtractWithDiacritics(): void
		{
			$wordExtractor = new WordExtractor();
			$result = $this->convertWords($wordExtractor->extract("Café", true, true, true));
			$this->assertEquals(["Café"], $result);
		}

		/**
		 * Tests that substrings in quotes are grouped into a subarray
		 */
		public function testExtractInQuotes(): void
		{
			$wordExtractor = new WordExtractor();
			$result = $this->convertWords($wordExtractor->extract('This "is exactly" what I mean!', true, true, true));
			$this->assertEquals(["This", ["is", "exactly"], "what", "I", "mean"], $result);
		}

		/**
		 * Tests that substrings in quotes are not grouped into a subarray when $groupExactWords is false
		 */
		public function testExtractInQuotesWithoutSubgroups(): void
		{
			$wordExtractor = new WordExtractor();
			$result = $this->convertWords($wordExtractor->extract('This "is exactly" what I mean!', false, false, true));
			$this->assertEquals(["This", "is", "exactly", "what", "I", "mean"], $result);
		}

		/**
		 * Tests that words starting with a dash are marked as excluded when exclude words is enabled
		 */
		public function testExtractExcluded(): void
		{
			$wordExtractor = new WordExtractor();
			$result = $wordExtractor->extract("-red", true, true, true);
			$this->assertTrue($result[0] instanceof Word && $result[0]->excluded);
		}

		/**
		 * Tests that substrings starting with a dash are marked as excluded when excluded words are enabled
		 */
		public function testExtractExcludedSubgroup(): void
		{
			$wordExtractor = new WordExtractor();
			$result = $wordExtractor->extract('-"red fox"', true, true, true);
			$this->assertTrue($result[0] instanceof WordGroup && $result[0]->excluded);
		}

		/**
		 * Tests that word alternates are extracted
		 */
		public function testExtractAlternatives(): void
		{
			$wordExtractor = new WordExtractor();
			$result = $wordExtractor->extract("red|blue fox", true, true, true);
			$this->assertTrue($result[0] instanceof WordAlternator);
		}

		/**
		 * Tests that word alternates are extracted inside subgroups
		 */
		public function testExtractAlternativesInsideSubgroup(): void
		{
			$wordExtractor = new WordExtractor();
			$result = $wordExtractor->extract('"red|blue fox"', true, true, true);
			$this->assertTrue($result[0] instanceof WordGroup && $result[0]->words[0] instanceof WordAlternator);
		}
	}