<?php
	namespace Unit\Words;

	use PHPUnit\Framework\TestCase;
	use Rizanola\Draconic\Words\Variant;
	use Rizanola\Draconic\Words\Word;
	use Wamania\Snowball\NotFoundException;
	use Wamania\Snowball\StemmerFactory;

	class WordTest extends TestCase
	{
		/**
		 * Tests that a word that is already normalised doesn't change
		 */
		public function testNormaliseNoChange(): void
		{
			$word = new Word("same", 0);
			self::assertEquals($word, $word->normalise());
		}

		/**
		 * Tests that a word with uppercase characters is lowercased
		 */
		public function testNormaliseLowerCasing(): void
		{
			$expected = new Word("lowercase", 0);
			$original = new Word("LoWErCaSE", 0);
			self::assertEquals($expected, $original->normalise());
		}

		/**
		 * Tests that dots are removed from an initialism
		 */
		public function testNormaliseRemoveDots(): void
		{
			$expected = new Word("eid", 0);
			$original = new Word("e.i.d", 0);
			self::assertEquals($expected, $original->normalise());
		}

		/**
		 * Tests that apostrophes are retained
		 */
		public function testNormaliseRetainApostrophes(): void
		{
			$word = new Word("couldn't've", 0);
			self::assertEquals($word, $word->normalise());
		}

		/**
		 * Tests that dashes are retained
		 */
		public function testNormaliseRetainDashes(): void
		{
			$word = new Word("extra-crispy", 0);
			self::assertEquals($word, $word->normalise());
		}

		/**
		 * Tests that the variant generator generates the expected variants for a simple word
		 */
		public function testGenerateVariantsForSimpleWord(): void
		{
			$word = new Word("hello", 0);
			$result = $word->generateVariants(1);
			$this->assertCount(5, $result);
		}

		/**
		 * Tests that the variant generator generates extra variants for words containing dashes
		 */
		public function testGenerateVariantsWithHyphen(): void
		{
			$word = new Word("speak-easy", 0);
			$result = $word->generateVariants();
			$this->assertContainsEquals(new Variant("speak", 0), $result);
			$this->assertContainsEquals(new Variant("easy", 0), $result);
		}

		/**
		 * Tests that the variant generator generates extra variants for words containing diacritics
		 */
		public function testGenerateVariantsWithDiacritic(): void
		{
			$word = new Word("café", 0);
			$result = $word->generateVariants();
			$this->assertContainsEquals(new Variant("cafe", 1), $result);
		}

		/**
		 * Tests that the variant generator generates extra "stem" variants when passed a stemmer
		 * @throws    NotFoundException    If the stemmer factory can't find "English"
		 */
		public function testGenerateVariantsWithStem(): void
		{
			$word = new Word("running", 0);
			$result = $word->generateVariants();
			$this->assertContainsEquals(new Variant("run", 1), $result);
		}
	}