<?php
	namespace Unit\Matching;

	use PHPUnit\Framework\TestCase;
	use Rizanola\Draconic\Matching\MatchedSection;
	use Rizanola\Draconic\Matching\MatchedWord;
	use Rizanola\Draconic\Words\Word;
	use Rizanola\Draconic\Words\WordGroup;

	class MatchedSectionTest extends TestCase
	{
		private MatchedSection $section;

		public function __construct(string $name)
		{
			parent::__construct($name);

			$this->section = new MatchedSection(2, "", [new MatchedWord("red", "red", 0, 0, 0), new MatchedWord("fox", "fox", 0, 1, 4), new MatchedWord("red", "red", 0, 2, 8), new MatchedWord("wolf", "wolf", 0, 4, 15)]);
		}

		/**
		 * Tests that the number of unique words is counted correctly
		 */
		public function testCountMatches(): void
		{
			$this->assertEquals(3, $this->section->countMatches());
		}

		/**
		 * Tests that the priority score is calculated correctly
		 */
		public function testGetPriorityScore(): void
		{
			$this->assertEquals(6, $this->section->getPriorityScore());
		}

		/**
		 * Tests that an actual sequence of words can be correctly identified
		 */
		public function testContainsSequenceMatches(): void
		{
			$wordGroup = new WordGroup([new Word("red", 0), new Word("fox", 4)], 0, 7);
			$this->assertTrue($this->section->containsSequence($wordGroup));
		}

		/**
		 * Tests that words out of order will be dismissed
		 */
		public function testContainsSequenceIncorrectOrder(): void
		{
			$wordGroup = new WordGroup([new Word("wolf", 0), new Word("red", 5)], 0, 8);
			$this->assertFalse($this->section->containsSequence($wordGroup));
		}

		/**
		 * Tests that non-contiguous words will be dismissed
		 */
		public function testContainsSequenceNonContiguous(): void
		{
			$wordGroup = new WordGroup([new Word("red", 0), new Word("wolf", 4)], 0, 8);
			$this->assertFalse($this->section->containsSequence($wordGroup));
		}
	}