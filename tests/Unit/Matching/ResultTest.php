<?php
	namespace Unit\Matching;

	use PHPUnit\Framework\TestCase;
	use Rizanola\Draconic\Matching\MatchedSection;
	use Rizanola\Draconic\Matching\MatchedWord;
	use Rizanola\Draconic\Matching\Result;

	class ResultTest extends TestCase
	{
		/**
		 * Tests that a result with no words returns false
		 */
		public function testContainsAllWithNoMatchingWords(): void
		{
			$result = new Result("test", null, [new MatchedSection(1, null, [new MatchedWord("white", "white", 0, 0, 0), new MatchedWord("wolf", "wolf", 0, 1, 7)])]);

			$this->assertFalse($result->containsAll(["red", "fox"]));
		}

		/**
		 * Tests that a result with some matching words returns false
		 */
		public function testContainsAllWithSomeMatchingWords(): void
		{
			$result = new Result("test", null, [new MatchedSection(1, null, [new MatchedWord("white", "white", 0, 0, 0), new MatchedWord("fox", "wolf", 0, 1, 7)])]);

			$this->assertFalse($result->containsAll(["red", "fox"]));
		}

		/**
		 * Tests that a result with all words in one section contains all words
		 */
		public function testContainsAllInOneSection(): void
		{
			$result = new Result("test", null, [new MatchedSection(1, null, [new MatchedWord("red", "red", 0, 0, 0), new MatchedWord("fox", "fox", 0, 1, 4)])]);

			$this->assertTrue($result->containsAll(["red", "fox"]));
		}

		/**
		 * Tests that a result with all words across multiple sections contains all words
		 */
		public function testContainsAllAcrossMultipleSections(): void
		{
			$result = new Result("test", null, [new MatchedSection(1, null, [new MatchedWord("red", "red", 0, 0, 0)]), new MatchedSection(1, null, [new MatchedWord("fox", "fox", 0, 0, 0)])]);

			$this->assertTrue($result->containsAll(["red", "fox"]));
		}

		/**
		 * Tests that the priority score gives the correct value for a result with one section
		 */
		public function testGetPriorityScoreForOneSection(): void
		{
			$result = new Result("test", null, [
				new MatchedSection(2, null, [
					new MatchedWord("red", "red", 0, 0, 0),
					new MatchedWord("fox", "fox", 0, 1, 4)
				])
			]);

			$this->assertEquals(4, $result->getPriorityScore());
		}

		/**
		 * Tests that the priority score gives the correct value for a result with multiple sections
		 */
		public function testGetPriorityScoreForMultipleSections(): void
		{
			$result = new Result("test", null, [
				new MatchedSection(3, null, [
					new MatchedWord("little", "little", 0, 0, 0)
				]),
				new MatchedSection(2, null, [
					new MatchedWord("red", "red", 0, 0, 0),
					new MatchedWord("fox", "fox", 0, 1, 4)
				])
			]);

			$this->assertEquals(7, $result->getPriorityScore());
		}

		/**
		 * Tests that the closeness score gives the correct value
		 */
		public function testGetClosenessScore(): void
		{
			$result = new Result("test", null, [
				new MatchedSection(1, null, [
					new MatchedWord("red", "rad", 1, 0, 0),
					new MatchedWord("fox", "fin", 2, 1, 4)
				])
			]);

			$this->assertEquals(1.5, $result->getExactnessScore());
		}
	}