<?php
	namespace Integration;

	use PHPUnit\Framework\TestCase;
	use Rizanola\Draconic\Database\Exceptions\ConnectionException;
	use Rizanola\Draconic\Database\Exceptions\QueryException;
	use Rizanola\Draconic\Database\QueryRetriever;
	use Rizanola\Draconic\Draconic;
	use Rizanola\Draconic\Entry;
	use Rizanola\Draconic\Section;
	use SQLite3;

	class DraconicTest extends TestCase
	{
		/**
		 * Runs through the code described in README.md
		 * @throws	ConnectionException		If the database fails to connect
		 * @throws	QueryException			If something goes wrong with the query
		 */
		public function testFullProcess(): void
		{
			$entry = new Entry("test-entry",
				"test",
				new Section("Test Heading", 2, "heading"),
				new Section("Test content", 1, "content")
			);

			$draconic = new Draconic(":memory:");
			$draconic->addOrUpdateEntries([$entry]);
			$results = $draconic->search('"test" content', "test");
			$draconic->removeEntries([$entry->id]);

			$this->assertCount(1, $results);
		}

		/**
		 * Checks that metadata is correctly stored and retrieved
		 * @throws	ConnectionException		If the database fails to connect
		 * @throws	QueryException			If something goes wrong with the query
		 */
		public function testMetadata(): void
		{
			$entry = new Entry("test-entry", "test", new Section("test", 1, "test"));
			$entry->setMetadata("test", "test");

			$draconic = new Draconic(":memory:");
			$draconic->addOrUpdateEntries([$entry]);

			$results = $draconic->search("test");
			$this->assertEquals("test", $results[0]->metadata->test);
		}

		/**
		 * Runs through the upgrade process
		 * @throws	ConnectionException		If the database fails to connect
		 * @throws	QueryException			If something goes wrong with the query
		 */
		public function testUpgradeProcess(): void
		{
			$path = sys_get_temp_dir() . "/" . uniqid() . ".sql";

			try
			{
				$queryRetriever = new QueryRetriever();
				$database = new SQLite3($path);

				$query = $queryRetriever->getQuery("setup-1");
				$database->query($query);

				$draconic = new Draconic($path);
				$results = $draconic->search("test");

				$this->assertCount(0, $results);
			}
			finally
			{
				unlink($path);
			}
		}
	}