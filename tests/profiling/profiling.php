<?php

	use joshtronic\LoremIpsum;
	use Rizanola\Draconic\Draconic;
	use Rizanola\Draconic\Entry;
	use Rizanola\Draconic\Section;

	require_once __DIR__ . "/../../vendor/autoload.php";

	$loremIpsum = new LoremIpsum();
	$entries = [];

	for($i = 0; $i < 100; $i += 1)
	{
		$id = $i;
		$title = $loremIpsum->sentence();
		$content = $loremIpsum->paragraph(5);

		$entries[] = new Entry($id, null,
			new Section($title, 2, "title"),
			new Section($content, 1, "content")
		);
	}

	$draconic = new Draconic("");
	$draconic->addOrUpdateEntries($entries);

